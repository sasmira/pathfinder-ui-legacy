# FoundryVTT - Pathfinder-ui-legacy v2
"Old School" v2 of Pathfinder Ui for PF1/PF2 and Swade, based on a dodgy fork of Fantasy Ui by Iotech.

## Installation

To install, follow these instructions:

1.  Inside Foundry, select the Game Modules tab in the Configuration and Setup menu.
2.  Click the Install Module button and enter the following URL: https://gitlab.com/sasmira/pathfinder-ui-legacy/-/raw/main/module/module.json
3.  Click Install and wait for installation to complete.

## Pathfinder Ui Legacy v2, it's :

1.  __**New icons for Pathfinder**__
2.  __**Chat colors**__
3.  __**Dicefinder**__, new dice for Pathfinder ! require [Dice-So-Nice](https://gitlab.com/riccisi/foundryvtt-dice-so-nice)
4.  __**Translation**__, French and English !

![Pathfinder-icons](Pathfinder-icons-v2.png)

![chat-colors](chat-colors.png)

![Dicefinder](Dicefinder-v2.png)


## Version & Compatibility

- Tested on 0.8.9 version.
