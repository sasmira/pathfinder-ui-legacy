# Patchnotes
## Compatibility
- Ready for v11 version.

## v11.306.4
- fix the problem of the interface shifting upwards.

## v11.306.3
- fix the problem overrides camera bar buttoms

## v11.306.2
- Fix buff icon position

## v11.306.1
- First release for Foundry VTT v11

## v10.284.1
- First release for Foundry VTT v10

## v2.3.7
- Initial Release for Pathfinder Legacy v2